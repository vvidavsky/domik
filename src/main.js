import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import VModal from "vue-js-modal";
import VeeValidate from "vee-validate";
import Notifications from "vue-notification"
import { VueSpinners } from "@saeris/vue-spinners"
import {store} from "@/globals/VuexConfig";
import ImageUploader from "vue-image-upload-resize";
import VueBus from 'vue-bus';
import Moment from "vue-moment";
import VTooltip from "v-tooltip";
import VueClazyLoad from 'vue-clazy-load';
import StarRating from 'vue-star-rating'


// eslint-disable-next-line no-unused-vars
import Directives from "./globals/Directives";

import Preloader from "@/components/Preloader";
import InputComp from "@/components/inputs/input-component";
import InputCompValidate from "@/components/inputs/input-component-validatable";
import DigitsComp from "@/components/inputs/digits-component";
import DigitsCompValidate from "@/components/inputs/digits-component-validatable";
import PickerComp from "@/components/inputs/picker-component";
import CheckboxComp from "@/components/inputs/checkbox-component";
import RadioComp from "@/components/inputs/radio-button-component";
import CurrencyPicker from "@/components/inputs/currency-picker";
import LabelComponent from '@/components/inputs/label-component';
import DatePickerComp from "@/components/inputs/date-picker";

Vue.config.productionTip = false;

Vue.use(VModal, {dynamic: true});
Vue.use(Notifications);
Vue.use(VeeValidate);
Vue.use(VueSpinners);
Vue.use(ImageUploader);
Vue.use(VueBus);
Vue.use(Moment);
Vue.use(VTooltip);
Vue.use(VueClazyLoad);

Vue.component("dm-input", InputComp);
Vue.component("dm-input-valid", InputCompValidate);
Vue.component("dm-digits-input", DigitsComp);
Vue.component("dm-digits-valid", DigitsCompValidate);
Vue.component("dm-picker", PickerComp);
Vue.component("dm-preloader", Preloader);
Vue.component("dm-checkbox", CheckboxComp);
Vue.component("dm-radio", RadioComp);
Vue.component("dm-currency-picker", CurrencyPicker);
Vue.component('dm-form-label', LabelComponent);
Vue.component('dm-date-picker', DatePickerComp);
Vue.component('dm-star-rating', StarRating);


new Vue({
    router,
    store,
    render: h => h(App)
}).$mount("#app");
