/**
 *
 * Written by vlad on 09/01/19
 */

import {Firebase} from '@/firebase/firebaseConf';

/**
 * @class Connection
 */
export default class Connection {

    /**
     * @constructor
     * @param {string} refName
     */
    constructor(refName) {

        /**
         * @property Connection
         * @type {string}
         */
        this.refName = refName;

        /**
         * @property Connection
         * @type {!firebase.database.OnDisconnect}
         */
        this.onDisconnectRef = this.ref().onDisconnect();

        this.isConnectedRef();
    }

    /**
     * @method ref
     * @param refName
     * @return {firebase.database.Reference}
     */
    ref(refName = this.refName) {
        if (!this.fbRef) {
            this.fbRef = Firebase.database().ref(refName);
        }
        return this.fbRef;
    }

    /**
     * @method isConnectedRef
     * @param connectedRef
     * @return {*}
     */
    isConnectedRef(connectedRef) {
        const ref = connectedRef || this.ref();
        return ref.on('value', snap => {
            if (snap && snap.val() === true) {
                console.log('>>> Connected', ref, snap);
            } else {
                Connection.onDisconnect();
            }
        });
    }

    /**
     * @method onDisconnect
     * @static
     */
    static onDisconnect() {
        if (this.onDisconnectRef) {
            this.onDisconnectRef.set('Disconnected');
            this.onDisconnectRef.cancel();
        }
    }
}