/**
 *
 * Written by vlad on 09/01/19
 */

import Firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/storage';
import 'firebase/firestore';

const config = {
    apiKey: "AIzaSyBo_DleVF4GSDrcOjAMkGTMticUH5rXkO8",
    authDomain: "domikmd-8e168.firebaseapp.com",
    databaseURL: "https://domikmd-8e168.firebaseio.com",
    projectId: "domikmd-8e168",
    storageBucket: "gs://domikmd-8e168.appspot.com",
    messagingSenderId: "737267036570"
};

Firebase.initializeApp(config);

const Firestore = Firebase.firestore();
export  {Firebase, Firestore};