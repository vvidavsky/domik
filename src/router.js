import Vue from "vue";
import Router from "vue-router";
import Home from "@/views/Home.vue";
import Profile from "@/views/profile/Profile.vue";
import PersonalDetails from "@/views/profile/PersonalDetails.vue";
import CreatePublication from "@/views/profile/CreatePublication.vue";
import CreateProject from "@/views/profile/CreateProject.vue";
import MyPublications from "@/views/profile/MyPublications.vue";
import Support from "@/views/profile/Support.vue";
import Rules from "@/views/profile/Rules.vue";
import PostFullView from "@/views/PostFullView.vue";
import Companies from "@/views/companies/Companies.vue";
import Company from "@/views/companies/Company.vue";

Vue.use(Router);


const router = new Router({
    //mode: 'history',
    routes: [
        {
            path: "/",
            name: "home",
            component: Home
        },
        {
            path: "/post-full-view/:property_type/:post_type/:post_id",
            name: "post-full-view",
            component: PostFullView
        },
        {
            path: "/companies",
            name: "companies",
            component: Companies,
            children: [
                {
                    path: "company/:id/",
                    name: "company",
                    component: Company
                }
            ]
        },
        {
            path: "/profile",
            name: "profile",
            component: Profile,
            children: [
                {
                    path: "personal-details",
                    name: "personal",
                    component: PersonalDetails
                },
                {
                    path: "create-new",
                    name: "create_publication",
                    component: CreatePublication
                },
                {
                    path: "create-project",
                    name: "create_project",
                    component: CreateProject
                },
                {
                    path: "my-publications",
                    name: "publications",
                    component: MyPublications
                },
                {
                    path: "support",
                    name: "support",
                    component: Support
                },
                {
                    path: "rules",
                    name: "rules",
                    component: Rules
                }
            ]
        }

    ]
});

// router.beforeEach((to, from, next) => {
//     // use the language from the routing param or default language
//     //to.params.lang = "en";
//     next();
// });

export default router;