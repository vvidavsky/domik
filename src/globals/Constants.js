/**
 *
 * Written by Vlad on 08/01/19
 */
import {uuid} from 'vue-uuid';

export const Constants = {
    MAP_TOKEN: "d2edaa86-3ea8-44f1-9361-dd99bb72fa1d",

    /*********** FIREBASE TABLES REFERENCES *************/
    APP_STRINGS: "strings/",
    FIRE_CITIES: "app_data/mold_cities/",
    FIRE_CONTACT_INFO: "contact_info",
    FIRE_S_USERS: "users",
    FIRE_S_COMPANIES_HARDCODED: "z_hardcoded_companies",
    FIRE_S_POSTS_BY_USER: "posts_by_user",
    FIRE_COMPANIES_REVIEWS: "reviews_for_companies",
    /********* & FIREBASE TABLES REFERENCES & ***********/

    /********* FIREBASE ERROR CODES ***********/
    FIRE_ERR_NO_USER: "auth/user-not-found",
    FIRE_ERR_WRONG_PASS: "auth/wrong-password",
    FIRE_ERR_EMAIL_IN_USE: "auth/email-already-in-use",
    /******** & FIREBASE ERROR CODES & ********/

    /************ APP CONFIGURATION *********************/
    MAX_ALLOWED_PHONES: 3,
    POST_LIFE_SPAN: 60, // Days
    PHONE_WIDTH: 600, //PX
    TABLET_WIDTH: 800, //PX
    MAX_ALLOWED_IMAGES: 10, //amount of allowed images for per post
    MAX_FILE_IMAGE_SIZE: 3, //maximal allowed image size to upload
    /********** & APP CONFIGURATION & *******************/

    /************ EVENT BUS NAMES ***********************/
    BUS_MOVE_MAP: "moveMap",
    BUS_SAVE_POST: "savePostDetails",
    BUS_HIGHLIGHT_POINTER: "highlightMapPointer",
    /********** & EVENT BUS NAMES & *********************/

    /************ MOMENT FORMATS ***********************/
    MT_FULL_DATE: "DD.MM.YYYY",
    /********** & MOMENT FORMATS & *********************/

    DEFAULT_PICKER_CITY: "chisinau",

    HEADER_LINKS: [
        {text: "to_main", router: "/"},
        {text: "companies", router: "/companies"}
    ],

    APP_LANGUAGES: [
        {
            type: "ro",
            text: "Română"
        },
        {
            type: "ru",
            text: "Русский"
        },
        {
            type: "en",
            text: "English"
        }
    ],

    PROFILE_LINKS: [
        {
            name: "personal_details",
            route: "/profile/personal-details",
            cls: "dm-personal-det-i"
        },
        {
            name: "new_publication",
            route: "/profile/create-new",
            cls: "dm-new-post-i"
        },
        {
            name: "new_project",
            route: "/profile/create-project",
            cls: "dm-new-project-i"
        },
        {
            name: "my_publications",
            route: "/profile/my-publications",
            cls: "dm-my-posts-i"
        },
        {
            name: "support",
            route: "/profile/support",
            cls: "dm-support-i"
        },
        {
            name: "rules",
            route: "/profile/rules",
            cls: "dm-rules-i"
        }
    ],

    MAIN_FILTERS_OBJECT: {
        post_type: "sale",
        property_type: "apartment",
        construction_type: "new_construction",
        condition: "euro_repair",
        photo: false,
        price: [0, 500000],
        price_pm: [0, 2000],
        property_size: [0, 300],
        rooms_amount: [1, 7]
    },
    ACC_TYPE_CONSTRUCTOR: "constructor",
    ACC_TYPE_AGENCY: "agency",
    ACC_TYPE_PRIVATE: "private",
    CONTACT_FIELDS: ["phone", "whatsapp", "telegram", "viber"],
    NEW_POST_SCHEME: {
        uuid: uuid.v4(),
        account_id: null,
        address: null,
        created_at_ts: null,
        renewed_at_ts: null,
        city: "chisinau",
        condition: "euro_repair",
        floor_num: null,
        floors_amount: null,
        currency: "€",
        construction_type: "new_construction",
        construction_year: null,
        post_type: "sale",
        property_type: "apartment",
        property_size: null,
        land_size: null,
        bathrooms: null,
        balcony: null,
        lat: null,
        long: null,
        geo_point: null,
        price: null,
        price_pm: null,
        rent_type: "long_term",
        rooms_amount: null,
        contact_name: null,
        approved: false,
        views_amount: 0,
        images: [],
        property_more_details: {
            parking: false,
            furniture: false,
            smart_home: false,
            video_surveillance: false,
            air_conditioner: false,
            appliances: false,
            autonomic_heating: false,
            lodge: false,
            floor_heating: false,
            fire_extinguishing: false,
            gas_leak_sensors: false
        },
        description: {
            ru: null,
            ro: null,
            en: null
        },
        complex_details: null,
        phone: [
            {
                code: null,
                number: null,
                uuid: uuid.v1()
            }
        ],
        whatsapp: [
            {
                code: null,
                number: null,
                uuid: uuid.v1()
            }
        ],
        telegram: [
            {
                code: null,
                number: null,
                uuid: uuid.v1()
            }
        ],
        viber: [
            {
                code: null,
                number: null,
                uuid: uuid.v1()
            }
        ]
    },
    COMPLEX_DETAILS: {
        lobby: false,
        lobby_guard: false,
        video_surveillance: false,
        fire_extinguishing: false,
        elevator: false,
        mall: false,
        pharmacy: false,
        atm: false,
        bank: false,
        gym: false,
        supermarket: false,
        underground_parking: false,
        ground_parking: false,
        beauty_salon: false,
        pool: false,
        car_wash: false,
        kinder_garden: false,
        play_ground: false,
    },
    BASIC_RENT_CONDITIONS: {
        no_pets: false,
        no_smoking: false,
        no_kids: false
    },
    DROPZONE_STRINGS: {
        dictDefaultMessage: {
            en: "DROP FILES HERE TO UPLOAD",
            ru: "ЗАГРУЗИТЬ ФОТОГРАФИИ",
            ro: ""
        },
        dictRemoveFile: {
            en: "REMOVE",
            ru: "УДАЛИТЬ",
            ro: ""
        },
        dictMaxFilesExceeded: {
            en: "You've reached maximum allowed images amount",
            ru: "Вы достигли максимально разрешенное количества фотографий",
            ro: ""
        },
        dictFileTooBig: {
            en: "File is too big, max file size is 3 MB.",
            ru: "Размер файла не должен превышать 3 МБ.",
            ro: ""
        }
    }
};