/**
 * Written by vlad on 09/01/2019
 */

import {store} from "@/globals/VuexConfig";
import {Constants} from "@/globals/Constants";

/**
 * Данный класс хранит инструмменты для общего пользования всеми компонентами
 * приложения. Вычисления, агригации и другая мелкая поеботина.
 * Влад. 09/01/19
 */
export default class AppTools {
    /**
     * Данная фунцтия получает объект с данными которы должен быть сохранен
     * проводит его валидацию и очищает по мере нужды, после чего возвращает
     * готовы объект с данными для сохранения
     * @param dataToOperate - объект с данными которому нужно провести проверку
     * @returns {*}
     * Влад. 27/01/19
     */
    static validatePersonalDataBeforeSave(dataToOperate) {
        Constants.CONTACT_FIELDS.map((field) => {
            const clonedFiled = [];
            dataToOperate[field].map(function (tel) {
                if ((tel.number && tel.number.length) || dataToOperate[field].length === 1) {
                    clonedFiled.push(tel);
                }
            });
            dataToOperate[field] = clonedFiled;
        });
        // Достаем таймстемп из объекта с календарной датой, и перезаписываем в тоже самое поле для сохранения.
        if (dataToOperate.establishment_date) {
            dataToOperate.establishment_date = new Date(dataToOperate.establishment_date).getTime();
        }
        return dataToOperate;
    }

    /**
     * Данная функция возвращает массив с координатами города
     * @param cityId - ID города чьи координаты нужно найти и вернуть
     * @returns {*[]}
     * Влад. 06/02/19
     */
    static getCityCoordinates(cityId) {
        const cities = store.state.cities;
        if (cities && cities.length) {
            const foundCity = cities.find((city) => {
                return city.id === cityId;
            });
            return [foundCity.long, foundCity.lat];
        }
    }

    /**
     * Данная функция производит вычисления стоимости недвижимости в зависимости
     * от параметров которые она получает, возможны вычисление: общая стоимость недвижимости либо
     * цена за кв.метер.
     * @param size - площадь недвижимости
     * @param price - общая стоимость недвижимости (опциональный параметр)
     * @param ppm - стоимость за квасратный метер (опциональный параметр)
     * @param valueToReturn - String обощначает какое значение нужно вычислить и вернуть
     * @returns {number}
     * Влад. 23/02/19
     */
    static calculatePropertyPrices(size, price, ppm, valueToReturn) {
        if (size && ((valueToReturn === "ppm" && price) || (valueToReturn === "price" && ppm))) {
            const a = parseInt(size);
            switch (valueToReturn) {
                case "ppm":
                    const b = parseInt(price);
                    return Math.floor(b / a);
                case "price":
                    const c = parseInt(ppm);
                    return Math.floor(a * c);
            }
        }
    }

    /**
     * Данная функция синхронизирует данные между пикером геолокации и заданным объектом данных и на оборот
     * когда объект с данными какого либо компонента получен из базы данных и в данном объекте находятся данные
     * геолокации то данные геолокации и аддреса синхронизируются с пикером местоположения данного экрана.
     * @param target объект данных который нужно синхронизировать
     * @param source объект с имеющимися данными с которым нужно синхронизироваться
     * Влад. 26/02/19
     */
    static syncLocationPickerData(target, source) {
        target.city = source.city.toLowerCase();
        target.address = source.address;
        target.lat = source.lat;
        target.long = source.long;
    }

    /**
     * Данная функция получает объект с персональными данными зарегистрированного пользователя
     * и проверяет есть ли в контактных полях хотябы один сохраненый номер телефона.
     * @param dataToCheck - объект с персональными данными пользователя.
     * @returns {boolean}
     * Влад. 07/03/19
     */
    static checkExistingContactDetails(dataToCheck) {
        return Constants.CONTACT_FIELDS.some((field) => {
            return dataToCheck[field][0].number.length;
        });
    }

    /**
     * Данная функция получает объект с данными объявления и возвращает объект со сгенерированными
     * линками на данное объявление во всех местах в базе данных для созхранения, удаления, редактирования.
     * @param postData объект с данными объявления
     * @returns {{postFullPath: string, postSlimPath: string, postsPerUserPath: string}}
     * Влад. 03/04/19
     */
    static getAllPostDbReferences(postData) {
        const pathFull = `${postData.property_type}_${postData.post_type}/full/posts/${postData.uuid}`;
        const pathSlim = `${postData.property_type}_${postData.post_type}/slim/posts/${postData.uuid}`;
        const pathToPostsByUser = `${Constants.FIRE_S_POSTS_BY_USER}/${postData.account_id}/posts/${postData.uuid}`;
        return {
            postFullPath: pathFull,
            postSlimPath: pathSlim,
            postsPerUserPath: pathToPostsByUser
        };
    }

    /**
     * У данной функции не множечко вынужденное предназночения изза не много долбоебского API библиотеки
     * DROPZONE, которая не умеет принимать тексты к своим инструментам асинхронно, поэтому все тексты предназначенные
     * для данной бибблиотеки хранятся в константах на трех языках и вытаскиваются от туда для использования для
     * инструментов DROPZONE.
     * @param tableName стринг название компонента/модуля для которого нужно получить текст
     * @returns {*}
     * Влад. 16/04/19
     */
    static getDropZoneStrings(tableName) {
        return Constants.DROPZONE_STRINGS[tableName][store.state.app_lang];
    }

    /**
     * Данная функция получает цену как параметр и возвращает ее же отформатированное значение.
     * С точками, запятыми и другой хуетой
     * @param price строка цены
     * @returns {*}
     * Влад. 20/05/19
     */
    static formatPrice(price) {
        return `${('' + price).replace(/\B(?=(\d{3})+(?!\d))/g, ',')} €`;
    }

    /**
     * Данная функция копирует любую полученную единицу данных и возвращает
     * индивидуальныю единицу данных без поинтеров на оригинальные данные
     * @param data - данные которые нужно склонировать
     * @returns {any}
     * Влад. 07/03/19
     */
    static clone(data) {
        return JSON.parse(JSON.stringify(data));
    }

    /**
     * Данная функция собирает все имеющиеся номера телефонов в одну группу форматируя данные и добавляя нужный
     * класс иконки в объект для удобной и эфективной итерации в момент рендеринга view.
     * @param data - является объектом содержащим разные типы номеров телефонов Вацап, Вайбер и хуё моё.
     * @returns {Array}
     * Влад. 05/06/19
     */
    static joinPhoneNumbersArrays(data) {
        const fieldsToJoin = Constants.CONTACT_FIELDS;
        const phonesListToReturn = [];
        fieldsToJoin.map(field => {
            if (data[field] && data[field].length) {
                data[field].map(phone => {
                    if (phone.number) {
                        const newPhoneObject = {
                            className: `dm-${field}-i`,
                            number: `${phone.code}-${phone.number}`
                        };
                        phonesListToReturn.push(newPhoneObject);
                    }
                });
            }
        });
        return phonesListToReturn;
    }

    /**
     * Данная функция получает объект содержащий поля и булеанное значение полей
     * и возвращает только те поля чье значение является true.
     * @param details - object with fields to check
     * @returns {string[]}
     * Влад. 06/06/19
     */
    static getOnlyTrueValues(details) {
        if (details) {
            return Object.keys(details).filter(field => details[field] === true);
        }
    }

    /**
     * Данная функция возвращает нужный текст описания объявление согласно языку приложения.
     * То есть если приложение на русском языке, то функция вернет русский текст
     * описания, если русский текст описания пуст то функция вернет первый найденый текст опицания на другом языке
     * румынском или английском. Если описание отсутствует на всех языках то функция нихуя блядь не вернет и графа ОПИСАНИЕ
     * будет пуста.
     * @param descData - массив содержащий описания на разных языках
     * @returns {{length}|*}
     * Влад. 06/06/19
     */
    static getRelevantPostDescription(descData) {
        let description = descData[store.state.app_lang];
        if (description && description.length) {
            return description;
        } else {
            const langsToCheck = Constants.APP_LANGUAGES.filter(lang => lang.type !== store.state.app_lang);
            if (langsToCheck && langsToCheck.length) {
                for (let i in langsToCheck) {
                    if (descData[langsToCheck[i].type] && descData[langsToCheck[i].type].length > 0) {
                        description = descData[langsToCheck[i].type];
                        break;
                    }
                }
            }
        }

    }
}