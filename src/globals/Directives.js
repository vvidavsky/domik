/**
 *
 * Written by vlad on 2019-05-28
 */

import Vue from "vue";
import router from "../router";
import {Constants} from "./Constants";
import AppTools from "../globals/CommonTools";

/**
 * Директива открывающая окно с полными данными объявления
 * передающая в url открытого окна параметры необходимые для
 * запроса полных данных объявления
 * Влад. 28/05/19
 */
Vue.directive("openPost", {
    inserted: function (el, object) {
        el.addEventListener("click", function () {
            const filters = Constants.MAIN_FILTERS_OBJECT;
            const link = router.resolve({path: `post-full-view/${object.value.property_type}/${filters.post_type}/${object.value.uuid}`});
            window.open(link.href, "_blank");
        });
    }
});
/**
 * Данная деректива меняет роутинг при нажатии на компанию в списке компаний тем самым открывая
 * полные детали нажатой компании.
 * Влад. 20/06/19
 */
Vue.directive("openCompany", {
    inserted: function (el, object) {
        el.addEventListener("click", function () {
            const id = object.value.id;
            router.replace({name: "company", params: {id}});
            // На маленьких экранах мы открываем попап с полными данными компании вместо
            // того чтоб показывать их в раоутер вью радом.
            if (window.outerWidth < Constants.TABLET_WIDTH) {
                object.value.dialog();
            }
        });
    }
});
/**
 * Директива форматирует полученную цену для того чтобы более удобно показать ее пользователю.
 * Влад. 15/06/19
 */
Vue.directive("formatPrice", {
    inserted: function (el, binding) {
        if (binding.value) {
            el.innerHTML = AppTools.formatPrice(binding.value);
        }
    },
    update: function (el, binding) {
        if (binding.value) {
            el.innerHTML = AppTools.formatPrice(binding.value);
        }
    }
});

/**
 * Данная деректива вписывает стринг с ценой за квадратный метер в элемент к которому она присвоена.
 * Влад. 15/06/19
 */
Vue.directive("getPpm", {
    inserted: function (el, binding) {
        if (binding.value) {
            el.innerHTML = `${binding.value} €/m${"\u00B2"}`;
        }
    },
    update: function (el, binding) {
        if (binding.value) {
            el.innerHTML = `${binding.value} €/m${"\u00B2"}`;
        }
    },

});