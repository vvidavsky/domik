/**
 *
 * Written by vlad on 19/11/2018
 */

import Vue from "vue";
import Vuex from "vuex";
import {Constants} from "@/globals/Constants";

Vue.use(Vuex);

/**
 * Here we hold all general application data accessible from any component
 * Vlad. 09/01/19
 * @type {Store}
 */
export const store = new Vuex.Store({
    state: {
        app_lang: window.localStorage.getItem("lang") || "ro",
        map_bounds: null,
        cities: null,
        main_app_spinner: null,
        async_request_sent: false,
        highlight_field: false,
        user_details: {},
        is_phone: window.innerWidth < Constants.PHONE_WIDTH,
        cached_personal_details: null, //Данные пользователя из страницы персональных данных
        postResults: null,
        hovered_post_id: null,
        companies_list: null,
        astrings: {
            login_dialog: {},
            links: {},
            personal_details: {},
            property_details: {},
            complex_details: {},
            header_links: {},
            rating_fields: {},
            notifications: {}
        }
    },
    getters: {
        //getInfoFbReference(state) {}
    },
    mutations: {
        setVuexHighlightField(state) {
            state.highlight_field = true;
            setTimeout(() => {
                state.highlight_field = false;
            }, 2000);
        },
        setVuexAppLanguage(state, lang) {
            state.app_lang = lang;
        },
        setVuexAppStrings(state, strings) {
            state.astrings = strings;
        },
        setVuexCities(state, cities) {
            state.cities = cities;
        },
        setVuexAppSpinner(state, value = null) {
            state.main_app_spinner = value;
        },
        setVuexUserDetails(state, details) {
            state.user_details = details;
        },
        setVuexAsyncRequestSent(state, value) {
            state.async_request_sent = value;
        },
        setVuexCacheForPersonalDetails(state, value) {
            state.cached_personal_details = value;
        },
        setVuexIsPhone(state, value) {
            state.is_phone = value;
        },
        setVuexPostResults(state, posts) {
            state.postResults = posts;
        },
        setVuexMapBounds(state, bounds) {
            state.map_bounds = bounds;
        },
        setVuexHoveredPostId(state, id) {
            state.hovered_post_id = id;
        },
        setVuexCompaniesData(state, companies) {
            state.companies_list = companies;
        }
    }
});