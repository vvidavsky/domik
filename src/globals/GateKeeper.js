/**
 *
 * Written by vlad on 2019-02-05
 */

import axios from "axios";

/**
 * Данный класс оперирует все HTTP Requests данного приложения
 * Влад 05/02/19
 */
export default class GateKeeper {
    /**
     * Данный метод отправляет HTTP Request и возвращает promise с полученными данными
     * @param method - post/get/put/delete etc...
     * @param url - String
     * @param data - Объект с данными для отправки с HTTP запросом
     * @returns {*}
     * Влад. 05/02/19
     */
    static fetch(method, url, data) {
        return axios[method](url, data).then((response) => {
            return response.data;
        });
    }
}